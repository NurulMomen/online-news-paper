<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Preview extends Model
{
    public function image(){
    	return $this->belongTo('App/Image');
    }
}
