<?php
Route::group(['prefix'=>'admin', 'middleware'=>'guest:admin'], function (){
    Route::get('/', 'Web\AdminController@showLoginForm')->name('admin.login');
    Route::post('/', 'Web\AdminController@login');
});

Route::group(['prefix'=>'admin', 'middleware'=>'auth:admin'], function (){
    Route::get('home', 'Web\AdminController@homeMethod')->name('admin.home');

    Route::get('profile', 'Web\AdminController@showProfileForm')->name('admin.profile');
    Route::put('profile', 'Web\AdminController@submitProfileForm');
    Route::get('password', 'Web\AdminController@showPasswordForm')->name('admin.password');
    Route::post('password', 'Web\AdminController@submitPasswordForm');

    Route::get('settings/general', 'Web\AdminController@showGeneralSettingsForm')->name('admin.settings_general');
    Route::put('settings/general', 'Web\AdminController@submitGeneralSettingsForm');

    Route::get('settings/media', 'Web\AdminController@showMediaSettingsForm')->name('admin.settings_media');
    Route::put('settings/media', 'Web\AdminController@submitMediaSettingsForm');

    Route::get('settings/headlines', 'Web\AdminController@showHeadlinesSettingForm')->name('admin.settings_headlines');
    Route::put('settings/headlines', 'Web\AdminController@submitHeadlinesSettingForm');

    Route::get('settings/headlines/sub', 'Web\AdminController@showSubHeadlinesSettingForm')->name('admin.settings_sub_headlines');
    Route::put('settings/headlines/sub', 'Web\AdminController@submitSubHeadlinesSettingForm');

    Route::get('settings/categories/menu', 'Web\AdminController@showMenuCategoriesForm')->name('admin.settings_menu_categories');
    Route::put('settings/categories/menu', 'Web\AdminController@submitMenuCategoriesForm');

    Route::get('settings/categories/front', 'Web\AdminController@showFrontCategoriesForm')->name('admin.settings_index_categories');
    Route::put('settings/categories/front', 'Web\AdminController@submitFrontCategoriesForm');

    Route::get('settings/categories/footer', 'Web\AdminController@showFooterCategoriesForm')->name('admin.settings_footer_categories');
    Route::put('settings/categories/footer', 'Web\AdminController@submitFooterCategoriesForm');

    Route::get('videos/create', 'Web\AdminController@showCreateVideoForm')->name('admin.videos.create');
    Route::post('videos', 'Web\AdminController@submitCreateVideoForm')->name('admin.videos.store');
    Route::get('videos', 'Web\AdminController@showAllVideos')->name('admin.videos.index');
    Route::get('videos/{video}/edit', 'Web\AdminController@showVideoEditForm')->name('admin.videos.edit');
    Route::put('videos/{video}', 'Web\AdminController@submitVideoEditForm')->name('admin.videos.update');
    Route::delete('videos/{video}', 'Web\AdminController@videoDeleteMethod')->name('admin.videos.destroy');

    Route::get('images/create', 'Web\AdminController@showCreateImageForm')->name('admin.images.create');
    Route::post('images', 'Web\AdminController@submitCreateImageForm')->name('admin.images.store');
    Route::get('images', 'Web\AdminController@showAllImages')->name('admin.images.index');
    Route::get('images/{image}/edit', 'Web\AdminController@showImageEditForm')->name('admin.images.edit');
    Route::put('images/{image}', 'Web\AdminController@submitImageEditForm')->name('admin.images.update');
    Route::delete('images/{image}', 'Web\AdminController@imageDeleteMethod')->name('admin.images.destroy');

    Route::get('categories/create', 'Web\AdminController@showCreateCategoryForm')->name('admin.categories.create');
    Route::post('categories', 'Web\AdminController@submitCreateCategoryForm')->name('admin.categories.store');
    Route::get('categories', 'Web\AdminController@showAllCategories')->name('admin.categories.index');
    Route::get('categories/{category}/edit', 'AdminController@showCategoryEditForm')->name('admin.categories.edit');
    Route::put('categories/{category}', 'AdminController@submitCategoryEditForm')->name('admin.categories.update');
    Route::delete('categories/{category}', 'AdminController@categoryDeleteMethod')->name('admin.categories.destroy');

    Route::get('editors/create', 'AdminController@showCreateEditorForm')->name('admin.editors.create');
    Route::post('editors', 'AdminController@submitCreateEditorForm')->name('admin.editors.store');
    Route::get('editors', 'AdminController@showAllEditors')->name('admin.editors.index');
    Route::get('editor/{editor}/edit', 'AdminController@showEditorEditForm')->name('admin.editors.edit');
    Route::put('editors/{editor}', 'AdminController@submitEditorEditForm')->name('admin.editors.update');
    Route::delete('editors/{editor}', 'AdminController@editorDeleteMethod')->name('admin.editors.destroy');

    Route::get('reporters/create', 'AdminController@showCreateReporterForm')->name('admin.reporters.create');
    Route::post('reporters', 'AdminController@submitCreateReporterForm')->name('admin.reporters.store');
    Route::get('reporters', 'AdminController@showAllReporters')->name('admin.reporters.index');
    Route::get('reporters/{reporter}/edit', 'AdminController@showReporterEditForm')->name('admin.reporters.edit');
    Route::put('reporters/{reporter}', 'AdminController@submitReporterEditForm')->name('admin.reporters.update');
    Route::delete('reporters/{reporter}', 'AdminController@reporterDeleteMethod')->name('admin.reporters.destroy');

    Route::get('news/create', 'AdminController@showCreateNewsForm')->name('admin.news.create');
    Route::post('news', 'AdminController@submitCreateNewsForm')->name('admin.news.store');
    Route::get('news', 'AdminController@showAllNews')->name('admin.news.index');
    Route::get('news/{news}/edit', 'AdminController@showNewsEditForm')->name('admin.news.edit');
    Route::put('news/{news}', 'AdminController@submitNewsEditForm')->name('admin.news.update');
    Route::delete('news/{news}', 'AdminController@newsDeleteMethod')->name('admin.news.destroy');

    Route::get('logout', 'Web\AdminController@logout')->name('admin.logout');
});

Route::group(['prefix'=>'editor', 'middleware'=>'guest:editor'], function (){
    Route::get('/', 'Web\EditorController@showLoginForm')->name('editor.login');
    Route::post('/', 'Web\EditorController@login');
});

Route::group(['prefix'=>'editor', 'middleware'=>'auth:editor'], function(){
    Route::get('home', 'Web\EditorController@homeMethod')->name('editor.home');

    Route::get('update/profile', 'Web\EditorController@showProfileForm')->name('editor.profile');
    Route::put('update/profile', 'Web\EditorController@submitProfileForm');
    Route::get('update/password', 'Web\EditorController@showPasswordForm')->name('editor.password');
    Route::put('update/password', 'Web\EditorController@submitPasswordForm');

    Route::get('news', 'Web\EditorController@showAllNews')->name('editor.news.index');
    Route::get('news/{news}/edit', 'EditorController@showNewsEditForm')->name('editor.news.edit');
    Route::put('news/{news}', 'EditorController@submitNewsEditForm')->name('editor.news.update');
    // Route::delete('news/delete/{newsId}', 'EditorController@newsDeleteMethod')->name('editor.delete.news');

    Route::get('images', 'EditorController@showAllImages')->name('editor.images.index');
    Route::get('images/{image}/edit', 'EditorController@showImageEditForm')->name('editor.images.edit');
    Route::put('images/{image}', 'EditorController@submitImageEditForm')->name('editor.images.update');

    Route::get('videos', 'EditorController@showAllVideos')->name('editor.videos.index');
    Route::get('videos/{video}/edit', 'EditorController@showVideoEditForm')->name('editor.videos.edit');
    Route::put('videos/{video}', 'EditorController@submitVideoEditForm')->name('editor.videos.update');

    Route::get('logout', 'EditorController@logout')->name('editor.logout');
});

Route::group(['prefix'=>'reporter', 'middleware'=>'guest:reporter'], function (){
    Route::get('/', 'ReporterController@showLoginForm')->name('reporter.login');
    Route::post('/', 'ReporterController@login')->name('reporter');
});

Route::group(['prefix'=>'reporter', 'middleware'=>'auth:reporter'], function(){
    Route::get('home', 'ReporterController@homeMethod')->name('reporter.home');

    Route::get('update/profile', 'ReporterController@showProfileForm')->name('reporter.profile');
    Route::put('update/profile', 'ReporterController@submitProfileForm');
    Route::get('update/password', 'ReporterController@showPasswordForm')->name('reporter.password');
    Route::put('update/password', 'ReporterController@submitPasswordForm');

    Route::get('news/create', 'ReporterController@showCreateNewsForm')->name('reporter.news.index');
    Route::post('news', 'ReporterController@submitCreateNewsForm')->name('reporter.news.store');
    Route::get('news', 'ReporterController@showAllNews')->name('reporter.news.index');
    Route::get('news/{news}/edit', 'ReporterController@showNewsEditForm')->name('reporter.news.edit');
    Route::put('news/{news}', 'ReporterController@submitNewsEditForm')->name('reporter.news.update');


    Route::get('images/create', 'ReporterController@showCreateImageForm')->name('reporter.images.create');
    Route::post('images', 'ReporterController@submitCreateImageForm')->name('reporter.images.store');
    Route::get('images', 'ReporterController@showAllImages')->name('reporter.images.index');
    Route::get('images/{image}/edit', 'ReporterController@showImageEditForm')->name('reporter.images.edit');
    Route::put('images/{image}', 'ReporterController@submitImageEditForm')->name('reporter.images.update');

    Route::get('videos/create', 'ReporterController@showCreateVideoForm')->name('reporter.videos.create');
    Route::post('videos', 'ReporterController@submitCreateVideoForm')->name('reporter.videos.store');
    Route::get('videos', 'ReporterController@showAllVideos')->name('reporter.videos.index');
    Route::get('videos/{video}/edit', 'ReporterController@showVideoEditForm')->name('reporter.videos.edit');
    Route::put('videos/{video}', 'ReporterController@submitVideoEditForm')->name('reporter.videos.update');

    Route::get('logout', 'ReporterController@logout')->name('reporter.logout');
});


    Route::get('/', 'FrontController@showIndexMethod')->name('front.index');
    Route::get('news/{newsId}', 'FrontController@showSpecificNews')->name('user.specific_category');
    Route::get('category/{categoryUrl}', 'FrontController@showCategoryNews')->name('user.specific_category');
    Route::get('image/{imageId}', 'FrontController@showSpecificImage')->name('user.specific_image');
    Route::get('video/{videoId}', 'FrontController@showSpecificVideo')->name('user.specific_video');

    Route::get('user', 'UserController@showLoginForm')->name('user.login');
    Route::post('user', 'UserController@login');

    Route::get('user/registration', 'UserController@showRegistrationForm')->name('user.register');
    Route::post('user/registration', 'UserController@register');

    Route::post('user/comment', 'UserController@submitCommentForm');

    Route::get('user/logout', 'UserController@logout')->name('user.logout');